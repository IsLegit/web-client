console.log('Test!')

var express = require('express');
var app = express();

var path = __dirname + '/src';
var port = 3300;

app.use(express.static(path));

app.get('*', function(req, res) {
    res.sendFile(path + '/index.html');
});

app.listen(port);
