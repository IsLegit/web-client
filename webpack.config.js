const webpack = require('webpack'),
      path = require('path'),
      ExtractTextPlugin = require("extract-text-webpack-plugin");

const inDevelopment = process.env.NODE_ENV !== "production";

/** Sass loader **/

const extractSass = new ExtractTextPlugin({
    filename: "./css/default.css",
    disable: process.env.NODE_ENV === "development"
});

function getSassLoader() {
  return {
    test: /\.s[ca]ss$/,
    loader: extractSass.extract({
      use: [
        { loader: "css-loader" },
        { loader: "sass-loader" }
      ],
      fallback: "style-loader"
    })
  };
}

/** Babel loader **/

function getBabelLoader() {
  const presets = [
    'es2015',
    'stage-0',
  ];

  const plugins = [
    'transform-decorators-legacy',
    'transform-class-properties',
    ['transform-react-jsx', { pragma: 'preact.h' }]
  ];

  return {
    test: /\.jsx?$/,
    exclude: /(node_modules|bower_components)/,
    loader: 'babel-loader',
    query: {
      presets: presets,
      plugins: plugins,
    }
  };
}

/** Define plugins **/

const decko = new webpack.ProvidePlugin({
  bind: ['decko', 'bind']
});

const dedupe = new webpack.optimize.DedupePlugin(),
      uglify = new webpack.optimize.UglifyJsPlugin({mangle: false, sourcemap: false}),
      production = new webpack.DefinePlugin({environment: '"production"', NODE_ENV: '"production"'});

function getPlugins() {
  const plugins = [
    extractSass,
    decko,
  ];

  if (!inDevelopment) {
    plugins.push(
      dedupe,
      uglify,
      production
    );
  }

  return plugins;
}

/** Export configuration **/

module.exports = {
  context: path.join(__dirname, "src"),
  entry: "./js/index.js",
  module: {
    loaders: [
      getBabelLoader(),
      getSassLoader(),
    ]
  },
  output: {
    path: __dirname + "/src/",
    filename: "app.js"
  },
  plugins: getPlugins(),
  devServer: {
    port: 3300,
    historyApiFallback: {
      index: 'index.html'
    }
  },
  resolve: {
    alias: {
      'modules': path.resolve(__dirname, 'src/js/modules'),
      'common': path.resolve(__dirname, 'src/js/common'),
      'network': path.resolve(__dirname, 'src/js/network'),
      'player': path.resolve(__dirname, 'src/js/modules/player/player'),
      "react": "preact-compat",
      "react-dom": "preact-compat",
      "react-addons-css-transition-group": "preact-css-transition-group"
    }
  },
};
