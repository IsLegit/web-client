(function(){
  let
  req = new XMLHttpRequest(),
  splashLogo = window.splashLogo = document.querySelector('#wd-init h1 img'),
  splashContainer = window.splashContainer = document.getElementById('wd-init'),
  splashProgressFill = window.splashProgressFill = document.querySelector('.wd-init__progressFill'),
  splashStatus = window.splashStatus = document.querySelector('.wd-init__status'),
  splashStatusText = window.splashStatusText = document.querySelector('.wd-init__statusText'),
  scripts = document.getElementsByClassName('wd-script');

  splashStatusText.innerHTML = 'Initializing...';

  window.destroySplash = function() {
    let transitionDuration = 300;

    splashContainer.style.transition = "ease all ".concat(transitionDuration, 'ms');
    splashContainer.setAttribute("class", "isComplete");

    setTimeout(function() {
      splashContainer.parentNode.removeChild(splashContainer);
    }, transitionDuration)
  }

  // Remove scripts
  while(scripts[0]) {
    scripts[0].parentNode.removeChild(scripts[0]);
  }

  // Fade in image
  splashLogo.addEventListener('load', function() {
    splashLogo.style.opacity = 1;
  })

  // report progress events
  req.addEventListener("progress", function(event) {
      if (event.lengthComputable) {
          let percentComplete = event.loaded / event.total;
          splashLogo.style.filter = 'saturate(' + percentComplete + ')';
          splashProgressFill.style.transform = 'scaleX(' + percentComplete + ')';
      } else {
          // Unable to compute progress information since the total size is unknown
      }
  }, false);

  // load responseText into a new script element
  req.addEventListener("load", function(event) {
    window.appCodeString = event.target.response;
    eval(event.target.response)
  }, false);

  req.open("GET", "/app.js");
  req.send();

  setTimeout(function() {
    if (splashStatus) {
      splashStatus.style.opacity = 1;
    }
  }, 3000);
}())
