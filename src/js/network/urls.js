const isTunneled = window.location.hostname !== 'localhost';

const CLIENT_URL = isTunneled ? 'client-wavedistrict.ngrok.io' : 'localhost:3300',
      API_URL = isTunneled ? 'api-wavedistrict.ngrok.io' : 'localhost:8000',
      MEDIA_URL = isTunneled ? 'media-wavedistrict.ngrok.io' : 'localhost:3030';

export {CLIENT_URL, API_URL, MEDIA_URL};
