import axios from 'axios';
import {observable} from 'mobx';

export default class RequestHandler {
  @observable endpoint = null;
  @observable numberOfRequests = null;

  constructor(endpoint, config, protocol) {
    this.protocol = protocol || 'http';
    this.endpoint = endpoint;
    this.numberOfRequests = 0;
    this.config = config;

    this.createInstance();
  }

  createInstance() {
    this.axios = axios.create({
      'baseURL': this.endpoint,
      ...this.config
    })
  }

  setConfig(config) {
    this.config = {
      ...this.config,
      ...config,
    };

    this.createInstance();
  }

  setHeaders(headers) {
    this.config = {
      ...this.config,
      'headers': {
        ...this.config.headers,
        ...headers,
      }
    };

    this.createInstance();
  }

  // Takes more than one argument
  request(method) {
    let
    args = Array.prototype.slice.call(arguments, 2),
    url = this.url + arguments[1],
    axios = this.axios,
    promise = axios[method](url, ...args);

    this.numberOfRequests += 1;

    const onComplete = () => {
      this.numberOfRequests -= 1;
    };

    promise.then(onComplete);
    promise.catch(onComplete);

    return promise;
  }

  // Method aliasing
  GET() {
    return this.request('get', ...arguments);
  }

  POST() {
    return this.request('post', ...arguments);
  }

  PUT() {
    return this.request('put', ...arguments);
  }

  DELETE() {
    return this.request('delete', ...arguments);
  }

  // G & S
  get url() {
    return this.protocol + '://' + this.endpoint;
  }
}
