import {action, observable} from 'mobx'
import moment from "moment";

import {ResourceStore} from 'common/classes/Store';
import {ApiServer} from 'network/http';

export default class ResourceList {
  Store = ResourceStore;

  url = '';
  filters = {};

  @observable count = null;
  @observable timestamp = null;

  @observable limit = 25;
  @observable offset = 0;

  constructor(url, Store, onAdd) {
    this.url = url;

    this.Store = Store || this.Store;
    this.onAdd = onAdd || this.Store.addItem;

    this.timestamp = moment().toISOString();
  }

  @action addItems(listOfItems) {
    return listOfItems.map(item => {
      return this.onAdd(item);
    })
  }

  /** Infinite scroll **/
  @observable items = [];
  @observable status = 'none';
  @observable hasInitial = false;
  @observable hasMore = false;

  @action handleSuccessfulResponse(response) {
    if (!response) throw new Error('Expected request response')
    if (!response.data || !response.data.results) throw new Error('Expected response results')

    this.items.push(...this.addItems(response.data.results));

    this.hasInitial = true;
    this.hasMore = !!response.data.next;

    this.status = 'fulfilled'
    this.offset = this.offset + this.limit;
    this.count = response.data.count;
  }

  @bind @action fetch() {
    const queries = {
      ...this.filters,
      'offset': this.offset,
      'limit': this.limit,
      'olderThan': this.timestamp,
    };

    this.status = 'pending';

    const promise = ApiServer.GET(this.url, {
      'params': queries
    });

    promise.then(response => {this.handleSuccessfulResponse(response)});

    return promise;
  }
}
