import {observable, computed, action} from 'mobx';
import Model from './Model';

export default class ResourceModel extends Model {
  @observable isFetched = false;
  @observable fetchStatus = 'none';
  @observable fetchError = null;

  @observable inDatabase = false;
}
