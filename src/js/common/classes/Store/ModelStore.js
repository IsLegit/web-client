import {observable, extendObservable, computed, action} from 'mobx';
import {Model} from 'common/classes/Model';

export default class ModelStore {
  @observable items = {};
  Model = Model;

  @action getOrCreateItem(id) {
    if (!this.items[id]) {
      extendObservable(this.items, {
        [id]: new this.Model(),
      })
    }

    return this.items[id];
  }

  @action renameItem(id, newId) {
    let
    item = this.getOrCreateItem(id),
    newItem = this.getOrCreateItem(newId);

    this.items[newId] = item;

    if (id !== newId) {
      delete this.items[id];
    }

    return newItem;
  }

  @action removeItem(id) {
    delete this.items[id];
  }
}
