import db from 'db.js';

export default class Database {
  constructor(name, version) {
    this.name = name;
    this.version = version;
    this.schema = {};
    this.listeners = [];

    this.isOpen = false;
  }

  registerTable(name, schema) {
    this.schema[name] = schema;
  }

  addListener(tableName, fn) {
    this.listeners.push({
      'tableName': tableName,
      'fn': fn
    });
  }

  triggerListeners() {
    this.listeners.map((listener) => {
      const table = this.connection[listener.tableName];
      if (table) listener.fn(table);
    });
  }

  open() {
    const promise = db.open({
      server: this.name,
      version: this.version,
      schema: this.schema,
    });

    promise.then((connection) => {
      this.connection = connection;
      this.isOpen = true;
      this.triggerListeners();
    });

    return promise;
  }
}
