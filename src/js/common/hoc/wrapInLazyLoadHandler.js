import preact from 'preact';
import bem from 'bem-classname';

import {isElementInViewport} from 'common/helpers/element';

const classNames = bem.bind(null, 'lazyLoadItem');

const wrapInLazyLoadHandler = (renderPlaceholder) => (Component) => {
  class LazyLoadHandler extends preact.Component {
    constructor(props) {
      super(props);

      this.state = {
        'canRender': false
      };

      this.getPlaceholderRef = this.getPlaceholderRef.bind(this);
      this.shouldRender = this.shouldRender.bind(this);
    }

    componentDidMount() {
      window.addEventListener('scroll', this.shouldRender, true);
      this.shouldRender();
    }

    componentWillUnmount() {
      window.removeEventListener('scroll', this.shouldRender, true);
    }

    shouldRender() {
      const
      {placeholderElement} = this,
      isVisible = isElementInViewport(placeholderElement, 200);

      this.setState({'canRender': isVisible});
    }

    getPlaceholderRef(ref) {
      this.placeholderElement = ref;
    }

    renderComponent() {
      if (this.state.canRender) return <Component {...this.props}/>;
    }

    render() {
      const rootStyle = {
        'position': 'relative'
      };

      const realStyle = {
        'position': 'absolute',
        'top': '0px',
        'left': '0px',
        'right': '0px'
      };

      const rootDivClassName = classNames({
        'isActive': this.state.canRender
      });

      return (
        <div style={rootStyle} ref={this.getPlaceholderRef} className={rootDivClassName}>
          <div className="lazyLoadItem__fake">{renderPlaceholder(this.props)}</div>
          <div className="lazyLoadItem__real">{this.renderComponent()}</div>
        </div>
      );
    }
  }

  return LazyLoadHandler;
}

export default wrapInLazyLoadHandler;
