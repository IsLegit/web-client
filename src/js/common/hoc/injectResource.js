import preact from 'preact';
import {observable} from 'mobx';
import {observer, inject} from 'mobx-react';

function renderLoadingPlaceholder() {
  return (
    <div className="loadingIndicator__fullHeight">
      <div className="loadingIndicator">
        <div className="loadingIndicator__pulse"></div>
        <div className="loadingIndicator__pulse"></div>
      </div>
    </div>
  );
}

const injectResource = (storesToInject, getResource, renderLoading = renderLoadingPlaceholder) => (Component) => {
  @inject(storesToInject) @observer
  class ResourceInjector extends preact.Component {
    @observable resource = null

    componentWillMount() {
      this.resource = getResource(this.props);
    }

    componentWillReceiveProps(nextProps) {
      this.resource = getResource(nextProps);
    }

    render() {
      if (!this.resource.isFetched) return renderLoading();

      return <Component {...this.props} resource={this.resource}/>;
    }
  }

  return ResourceInjector;
}

export default injectResource;
