import preact from 'preact';
import {Redirect} from 'react-router-dom';

// Expects redirectTo to return either a null value or a string

const redirect = (redirectTo) => (Component) => {
  class RedirectHandler extends preact.Component {
    render() {
      const
      redirectDestination = redirectTo(this.props),
      shouldRedirect = !!redirectDestination;

      if (shouldRedirect) {
        return <Redirect to={redirectDestination}/>
      }

      return (
        <Component {...this.props}/>
      )
    }
  }

  return RedirectHandler;
}

export default redirect;
