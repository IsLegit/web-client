
// Processing
import {renderContainedImage} from './renderContainedImage';
import {renderCroppedImage} from './renderCroppedImage';
import {renderBlurredImage} from './renderBlurredImage';

const imageProcessor = {
  renderContainedImage,
  renderCroppedImage,
  renderBlurredImage,
};

export {imageProcessor};
