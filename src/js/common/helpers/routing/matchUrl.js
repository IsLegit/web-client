export function matchUrl(toMatch, matchee) {
  return (
    toMatch == matchee ||
    toMatch == matchee.concat('/')
  );
}
