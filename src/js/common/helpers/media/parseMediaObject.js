export default function parseMediaObject(media = {}, fields, defaults = {}) {
  let obj = {}

  // Map through fields
  fields.forEach(field => {
    let result = {};
    const mediaObj = media[field];

    // Media exist, map it
    if (mediaObj) mediaObj.forEach((item) => {
      result[item['name']] = item['url'];
    });

    // Media doesn't exist, use default
    if (!mediaObj) result = defaults[field] || {};

    // Set if the thing existed or not
    result.exists = !!mediaObj;

    // Finally, set the result
    obj[field] = result;
  })

  return {...obj}
}
