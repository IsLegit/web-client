export function isElementInViewport(el, offset = 0) {
  var rect = el.getBoundingClientRect();

  return (
    rect.bottom + offset > 0 &&
    rect.right + offset > 0 &&
    rect.left - offset < (window.innerWidth || document.documentElement.clientWidth) &&
    rect.top - offset < (window.innerHeight || document.documentElement.clientHeight)
  );
}
