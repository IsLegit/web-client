export function traverseElement(el, callback) {
  while (el.parentNode) {
    callback(el.parentNode)
    el = el.parentNode;
  }
}
