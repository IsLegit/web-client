import preact from 'preact';
import {NavLink} from 'react-router-dom';

export default class LinkedTabs extends preact.Component {
  renderTabs() {
    return this.props.tabs.map((tab) => {
      return (
        <NavLink key={tab.path} to={tab.path} className="tabLink" activeClassName="isActive">
          {tab.label}
        </NavLink>
      );
    })
  }

  render() {
    return (
      <div className="tabRow">
        {this.renderTabs()}
      </div>
    )
  }
}
