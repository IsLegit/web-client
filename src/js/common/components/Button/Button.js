import preact from 'preact';
import bem from 'bem-classname';

import Icon from 'common/components/Icon';

// Bind classsnames
const classNames = bem.bind(null, 'button');

export default class Button extends preact.Component {

  @bind
  handleClick() {
    const
    {onClick} = this.props;
    if (onClick) onClick();
  }

  renderContent() {
    const
    {label, icon} = this.props,
    renderedContentArray = [];

    if (this.props.children.length) return this.props.children;

    if (icon) renderedContentArray.push(
      <div className={classNames('icon')}>
        <Icon name={icon}/>
      </div>
    );

    if (label) renderedContentArray.push(
      <div className={classNames('label')}>{label}</div>
    );

    return renderedContentArray;
  }

  render({isActive, type, modifiers}) {

    // Set default type
    type = type || 'default';

    const buttonClasses = bem('button', {
      [type]: true,
      isActive: isActive,
      ...modifiers,
    });

    const buttonProps = {
      'className': buttonClasses,
      'onClick': this.handleClick,
    };

    const children = this.renderContent();

    return (
      <a role="button" {...buttonProps}>
        {children}
      </a>
    )
  }
}
