import preact from 'preact';
import classNames from 'classnames';

import {observable, action} from 'mobx';
import {observer, inject} from 'mobx-react';

@observer
class CheapImage extends preact.Component {
  constructor() {
    super();

    this.setState({
      'hasLoaded': false,
    });

    this.image = document.createElement('img');
  }

  componentWillMount() {
    this.image.src = this.props.src;
    this.image.onload = this.handleImageLoad;
  }

  @bind
  handleImageLoad() {
    this.setState({
      'hasLoaded': true,
    });
  }

  getStyle() {
    const style = {
      'background-size': this.props.scaling || 'contain',
      'width': this.props.width,
      'height': this.props.height,
      'background-image': `url(${this.image.src})`
    };

    return style;
  }

  render() {
    const {src} = this.props;

    const imageClasses = classNames({
      'cheapImage': true,
      'hasLoaded': this.state.hasLoaded
    });

    return (
      <div style={this.getStyle()} className={imageClasses}/>
    )
  }
}

export default CheapImage;
