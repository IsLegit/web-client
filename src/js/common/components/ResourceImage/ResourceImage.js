import preact from 'preact';
import {observer, inject} from 'mobx-react';

import Image from 'common/components/Image';
import CheapImage from 'common/components/CheapImage';

@observer
export default class ResourceImage extends preact.Component {
  constructor(props) {
    super(props);

    this.state = {
      'hasSize': false
    };

    this.getRef = this.getRef.bind(this);
  }

  componentDidMount() {
    this.getSize();
    this.setState({'hasSize': true})
  }

  getRef(ref) {
    this.sizeRef = ref;
  }

  getSize() {
    if (this.props.size == 'auto') {
      const
      ref = this.sizeRef,
      size = Math.max(ref.offsetWidth, ref.offsetHeight);

      if (size > 500) return 'big';
      if (size > 200) return 'normal';
      if (size > 50) return 'small';
      if (size <= 50) return 'tiny';
    }

    return this.props.size;
  }

  renderExpensiveImage() {
    let
    {scalingMethod, resource, name, size} = this.props,
    media = resource.media[name];

    return (
      <Image {...this.props} main={media[this.getSize()]} preload={media.preload}/>
    )
  }

  renderCheapImage() {
    let
    {resource, name} = this.props,
    media = resource.media[name];

    return (
      <CheapImage {...this.props} src={media[this.getSize()]}/>
    )
  }

  render() {
    let {size, cheap} = this.props;

    // Empty placeholder to get container width
    if (!this.state.hasSize && size == 'auto') {
      return (
        <div ref={this.getRef} style={{
          'width': '100%',
          'height': '100%',
        }}/>
      )
    }

    if (cheap) return this.renderCheapImage();
    return this.renderExpensiveImage();
  }
}
