import preact from 'preact';
import {observer, inject} from 'mobx-react';

import Button from 'common/components/Button';

@observer
export default class InfiniteScrollList extends preact.Component {
  constructor(props) {
    super(props);

    this.state = {
      'limitMultiplier': 1,
    };

    this.resourceList = props.resourceList;
    this.renderList = props.renderList;
  }

  shouldFetchInitial() {
    const fetch = this.props.fetch || this.resourceList.fetch;

    if (!this.resourceList.hasInitial) {
      fetch();
    }
  }

  componentDidMount() {
    this.shouldFetchInitial();
  }

  @bind handleMoreClick() {
    const {scrollContainer} = this.context;
    const {scrollToBottom} = this.props;

    this.resourceList.fetch();

    if (scrollContainer && scrollToBottom) {
      scrollContainer.scrollToBottom();
    }
  }

  @bind
  renderMoreButton(resourceList) {
    const {buttonType} = this.props;

    const modifiers = {
      'loadMore': true
    };

    if (resourceList.status == 'pending') {
      return (
        <div className="infiniteScrollList__loadingIndicator">
          <div className="loadingIndicator">
            <div className="loadingIndicator__pulse"></div>
            <div className="loadingIndicator__pulse"></div>
          </div>
        </div>
      );
    }

    if (resourceList.hasMore) {
      return (
        <div className="infiniteScrollList__moreButtonContainer">
          <Button
            label="Load more"
            onClick={this.handleMoreClick}
            type={buttonType || "default"}
            modifiers={modifiers}
          />
        </div>
      )
    }

    return null;
  }

  render() {
    const {
      resourceList,
      renderList,
      renderMoreButton,
      render
    } = this;

    return (
      <div className="infiniteScrollList">
        {renderList(resourceList.items)}
        {renderMoreButton(resourceList)}
      </div>
    );
  }
}
