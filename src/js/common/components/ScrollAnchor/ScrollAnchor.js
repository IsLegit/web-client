import preact from 'preact';

export default class ScrollAnchor extends preact.Component {
  @bind
  getRef(ref) {
    this.itemRef = ref;
  }

  scrollToThis() {
    const {scrollContainer} = this.context;

    if (scrollContainer.scrollbars) {
      scrollContainer.scrollbars.scrollTop(this.itemRef.offsetTop);
    }
  }

  componentDidMount() {
    if (this.props.isActive) this.scrollToThis();
  }

  render() {
    return (
      <div ref={this.getRef} className="scrollAnchor">
        {this.props.children}
      </div>
    )
  }
}
