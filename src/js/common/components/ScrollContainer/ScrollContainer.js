import preact from 'preact';
import {Scrollbars} from 'preact-custom-scrollbars';
import bem from 'bem-classname';

const classNames = bem.bind(null, 'scrollContainer');

export default class ScrollContainer extends preact.Component {
  @bind renderView(props) {
    return (
      <div
        {...props}
        className={classNames('inner')}
        style={{...props.style, position: 'relative'}}
      />
    );
  }

  getChildContext() {
    return {
      'scrollContainer': this
    }
  }

  @bind
  getScrollbarRef(scrollbars) {
    this.scrollbars = scrollbars;
  }

  render() {
    const renderProps = {
      'renderView': this.renderView
    };

    return (
      <Scrollbars ref={this.getScrollbarRef} {...this.props} {...renderProps}>
        <div className={classNames('viewContainer')}>
          {this.props.children}
        </div>
      </Scrollbars>
    )
  }
}
