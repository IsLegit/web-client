import preact from 'preact';

export function play() {
  return (
    <svg id="svg2" viewBox="0 0 50.000001 50.000001" height="50" width="50" version="1.1">
      <path id="path3338" d="m10 50c13.333-8.334 26.667-16.666 40-25-13.333-8.334-26.667-16.666-40-25v50z" fillRule="evenodd" fill="#fff" />
    </svg>
  )
}

export function pause() {
  return (
    <svg id="svg2" height="50" width="50" version="1.1" viewBox="0 0 50.000001 50.000001">
      <path id="rect4791" fill="#fff" d="m5 0h12.5v50h-12.5z" />
      <path id="rect4791-6" fill="#fff" d="m32.5 0h12.5v50h-12.5z" />
    </svg>
  )
}

export function queue() {
  return (
    <svg fill="#FFFFFF" height="24" viewBox="0 0 24 24" width="24">
      <path d="M15 6H3v2h12V6zm0 4H3v2h12v-2zM3 16h8v-2H3v2zM17 6v8.18c-.31-.11-.65-.18-1-.18-1.66 0-3 1.34-3 3s1.34 3 3 3 3-1.34 3-3V8h3V6h-5z"/>
    </svg>
  )
}

export function next() {
  return (
    <svg fill="#FFFFFF" height="50" viewBox="0 0 24 24" width="50">
      <path d="M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z"></path>
    </svg>
  )
}

export function previous() {
  return (
    <svg fill="#FFFFFF" height="50" viewBox="0 0 24 24" width="50">
      <path d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z"></path>
    </svg>
  )
}

export function shuffle() {
  return (
    <svg fill="#FFFFFF" version="1.1" width="24" height="24" viewBox="0 0 24 24">
      <path d="M14.83,13.41L13.42,14.82L16.55,17.95L14.5,20H20V14.5L17.96,16.54L14.83,13.41M14.5,4L16.54,6.04L4,18.59L5.41,20L17.96,7.46L20,9.5V4M10.59,9.17L5.41,4L4,5.41L9.17,10.58L10.59,9.17Z"/>
    </svg>
  )
}

export function repeat() {
  return (
    <svg fill="#FFFFFF" version="1.1" width="24" height="24" viewBox="0 0 24 24">
      <path d="M17,17H7V14L3,18L7,22V19H19V13H17M7,7H17V10L21,6L17,2V5H5V11H7V7Z"/>
    </svg>
  )
}

export function volume() {
  return (
    <svg fill="#FFFFFF" version="1.1" width="24" height="24" viewBox="0 0 24 24">
      <path d="M14,3.23V5.29C16.89,6.15 19,8.83 19,12C19,15.17 16.89,17.84 14,18.7V20.77C18,19.86 21,16.28 21,12C21,7.72 18,4.14 14,3.23M16.5,12C16.5,10.23 15.5,8.71 14,7.97V16C15.5,15.29 16.5,13.76 16.5,12M3,9V15H7L12,20V4L7,9H3Z"/>
    </svg>
  )
}
