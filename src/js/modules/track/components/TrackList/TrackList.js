import preact from 'preact';

// HOC
import {wrapInListHandler} from 'common/hoc';
import {injectQueue} from 'modules/player/hoc';

// Components
import TrackListItem from 'modules/track/components/TrackListItem';

@wrapInListHandler
@injectQueue((props) => {
  const
  {preventQueue, resourceList, items, queueName} = props;

  if (!preventQueue) return {
    'name': queueName,
    'endpoint': resourceList.url,
    'timestamp': resourceList.timestamp,
  };
})
export default class TrackList extends preact.Component {
  render() {
    const
    renderedList = this.props.items.map((resource, index) => {
      return <TrackListItem index={index} key={resource.data.id} resource={resource}/>;
    })

    return (
      <ul className="trackList">
          {renderedList}
      </ul>
    );
  }
}
