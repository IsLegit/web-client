import {ResourceList} from 'common/classes/List';

// Stores
import {TrackStore} from 'modules/track/stores';

class ExploreStore {
  constructor() {
    this.tracks = new ResourceList('/tracks/', TrackStore);
  }
}

const store = new ExploreStore;
export default store;
