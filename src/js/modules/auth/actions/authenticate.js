import {ApiServer} from 'network/http';

import {AuthStore} from 'modules/auth/stores';
import {UserStore} from 'modules/user/stores';

export default function authenticate() {
  return new Promise((resolve, reject) => {
    if (!localStorage.authToken) {
      return resolve();
    }

    window.splashStatusText.innerHTML = 'Authenticating...';

    ApiServer.setHeaders({
      'Authorization': 'Token ' + localStorage.authToken
    });

    ApiServer.GET('/auth/user/')
    .then((response) => {
      AuthStore.currentUser = UserStore.addItem(response.data.user);

      window.splashStatusText.innerHTML = 'Logged in as ' + AuthStore.currentUser.data.display_name;
      resolve();
    })
  })
}
