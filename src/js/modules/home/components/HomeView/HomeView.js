import preact from 'preact';

// Components
import SliderInput from 'common/components/SliderInput';
import Button from 'common/components/Button';

// HOC
import injectCurrentUser from 'modules/auth/hoc/injectCurrentUser';

@injectCurrentUser
export default class HomeView extends preact.Component {
  constructor() {
    super();

    this.state = {
      'active': false
    };
  }

  @bind
  handleClick() {
    this.setState({active: !this.state.active});
  }

  render() {
    return (
      <div style={{padding: '20px'}}>
        <SliderInput/>
        <Button onClick={this.handleClick} label="Test!" isActive={this.state.active}/>
      </div>
    )
  }
}
