import {observable} from 'mobx';

class ApplicationStore {
  @observable isReady = false;

  @observable config = {};
  @observable metaData = {};

  rootComponent = null;
}

export default new ApplicationStore;
