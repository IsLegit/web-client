import preact from 'preact';
import {observer, inject} from 'mobx-react';

// Components
import Header from 'modules/core/components/Header';
import Body from 'modules/core/components/Body';

// Player components
import Transport from 'modules/player/components/Transport';

// Overlay components
import PopoverOverlay from 'modules/popover/components/PopoverOverlay';

// Router
import {withRouter} from 'react-router-dom';

const scrollingElement = document.scrollingElement || document.body;

@withRouter @inject('ApplicationStore') @observer
export default class ApplicationView extends preact.Component {
  constructor() {
    super();
    this.handleMouseMove = this.handleMouseMove.bind(this);
  }

  handleMouseMove(event) {
    window.mouseX = event.clientX;
    window.mouseY = event.clientY;
  }

  componentDidMount() {
    window.addEventListener('mousemove', this.handleMouseMove);
  }

  /** Proxy context **/

  getChildContext() {
    return {
      'scrollContainer': this
    }
  }

  /** Proxy methods **/

  scrollTo(pos) {
    scrollingElement.scrollTop = pos;
  }

  scrollToBottom() {
    scrollingElement.scrollTop = scrollingElement.scrollHeight;
  }

  /** Component **/

  render() {
    const {ApplicationStore, children, location} = this.props;

    return (
      <div id="wd-app" className="wavedistrict-app">
        <div className="wrapper">
          <Header {...this.props}/>
          <div className="body__wrapper">
            <Body {...this.props} routerChildren={children}/>
          </div>
          <Transport/>
        </div>
        <div className="overlays">
          <PopoverOverlay/>
        </div>
      </div>
    )
  }
}
