import preact from 'preact';
import {Provider} from 'mobx-react';

import {ApplicationStore} from 'modules/core/stores';
import {AuthStore} from 'modules/auth/stores';
import {UserStore} from 'modules/user/stores';
import {TrackStore} from 'modules/track/stores';
import {PopoverStore} from 'modules/popover/stores';
import {ExploreStore} from 'modules/explore/stores';

const Stores = {
  ApplicationStore,
  AuthStore,
  UserStore,
  TrackStore,
  PopoverStore,
  ExploreStore,
};

export default class StoreProvider extends preact.Component {
  render() {
    return (
      <Provider {...Stores}>
        {this.props.children}
      </Provider>
    )
  }
}
