import preact from 'preact';
import classNames from 'classnames';

export default class NavButton extends preact.Component {
  render() {
    const classes = classNames(
      'masthead__navButton', {
        'isActive': this.props.isPopoverActive,
      }
    );

    return (
      <div className={classes}>
        <div className="masthead__navButtonContent">
          {this.props.content}
        </div>
      </div>
    )
  }
}
