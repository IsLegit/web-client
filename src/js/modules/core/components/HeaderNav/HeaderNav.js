import preact from 'preact';
import {observer, inject} from 'mobx-react';

// Components
import TooltipWrapper from 'common/components/TooltipWrapper';
import FixedPopoverTrigger from 'common/components/FixedPopoverTrigger';
import ResourceImage from 'common/components/ResourceImage';
import Icon from 'common/components/Icon';

import NavButton from './NavButton'

// Routing
import {NavLink} from 'react-router-dom';
import {getHeaderLinks} from 'modules/core/helpers/header';

// HOC
import injectCurrentUser from 'modules/auth/hoc/injectCurrentUser';

@injectCurrentUser
@observer
export default class HeaderNav extends preact.Component {
  renderNavLink(path, icon, text, condition, isExact) {
    if (condition) return (
      <TooltipWrapper key={path} text={text}>
        <NavLink exact={isExact} to={path} className="wd-link masthead__navLink" activeClassName="selected">
          <div className="masthead__navLinkIcon">
            <Icon name={icon}/>
          </div>
          <div className="masthead__navLinkText">{text}</div>
        </NavLink>
      </TooltipWrapper>
    );

    return null;
  }

  renderPopoverButton(title, content, HeaderPopoverPanel) {
    const renderPopoverContent = () => {
      return (
        <div className="masthead__popover">
          <div className="popover__title">{title}</div>
          <div className="masthead__popoverContent">
            Not implemented
          </div>
        </div>
      );
    };

    return (
      <TooltipWrapper key={title.toLowerCase()} text={title}>
        <FixedPopoverTrigger isFixed renderPopoverContent={renderPopoverContent}>
          <NavButton content={content}/>
        </FixedPopoverTrigger>
      </TooltipWrapper>
    );
  }

  renderLinks() {
    return getHeaderLinks().map(link => {
      return this.renderNavLink(link.path, link.icon, link.text, link.condition, link.isExact);
    })
  }

  renderButtons() {
    const
    user = this.props.authUser,
    buttons = [];

    if (user) {
      const UserAvatar = (
        <div className="masthead__userAvatar">
          <ResourceImage height="100%" width="100%" resource={this.props.authUser} name="avatar" size="small"/>
        </div>
      );

      buttons.push(
        this.renderPopoverButton('User menu', UserAvatar, null),
        this.renderPopoverButton('Notifications', <Icon name="app.bell"/>, null)
      )
    }

    return buttons;
  }

  render() {
    return (
      <nav className="masthead__nav">
        <div className="masthead__links">{this.renderLinks()}</div>
        <div className="masthead__buttons">{this.renderButtons()}</div>
      </nav>
    )
  }
}
