// Shortcut to the player instance

import Player from 'modules/player/classes/Player';
const playerInstance = new Player();

export default playerInstance;
