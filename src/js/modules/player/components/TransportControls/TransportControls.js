import preact from 'preact';
import Player from 'player';
import {observer, inject} from 'mobx-react';

import Button from 'common/components/Button';
import PlayButton from 'modules/player/components/PlayButton';

@observer
export default class TransportControls extends preact.Component {
  renderRepeatButton() {
    return (
      <Button icon="player.repeat" onClick={Player.toggleRepeat} isActive={Player.isRepeating} type="playerTransportButton"/>
    )
  }

  renderPreviousButton() {
    return (
      <Button icon="player.previous" onClick={Player.previous} type="playerTransportButton"/>
    )
  }

  renderPlayButton() {
    return (
      <PlayButton
        className="playButton playButton--small"
        resource={this.props.resource}
      />
    )
  }

  renderNextButton() {
    return (
      <Button icon="player.next" onClick={Player.next} type="playerTransportButton"/>
    )
  }

  renderShuffleButton() {
    return (
      <Button icon="player.shuffle" onClick={Player.toggleShuffle} isActive={Player.isShuffling} type="playerTransportButton"/>
    )
  }

  render() {
    const {resource} = this.props;

    return (
      <div className="playerControls">
        <div className="playerControls__repeat">
          {this.renderRepeatButton()}
        </div>
        <div className="playerControls__prev">
          {this.renderPreviousButton()}
        </div>
        <div className="playerControls__toggle">
          {this.renderPlayButton()}
        </div>
        <div className="playerControls__next">
          {this.renderNextButton()}
        </div>
        <div className="playerControls__shuffle">
          {this.renderShuffleButton()}
        </div>
      </div>
    )
  }
}
