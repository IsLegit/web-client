import preact from 'preact';
import bem from 'bem-classname';
import {observer, inject} from 'mobx-react';

// Components
import Button from 'common/components/Button';
import InfiniteScrollList from 'common/components/InfiniteScrollList';
import ScrollAnchor from 'common/components/ScrollAnchor';
import Icon from 'common/components/Icon';

// Related components
import TrackItem from './TrackItem';

const classNames = bem.bind(null, 'queueItem');

@observer
export default class QueueItem extends preact.Component {
  constructor(props) {
    super();

    this.state = {
      'isCollapsed': !props.isActive,
    };
  }

  @bind
  renderTracks(items) {
    const {queueIndex, item} = this.props;

    return items.map((resource, trackIndex) => {
      const isActive = (trackIndex == item.index) && this.props.isActive;

      return (
        <ScrollAnchor isActive={isActive}>
          <TrackItem
            queueIndex={queueIndex}
            index={trackIndex}
            isActive={isActive}
            resource={resource}
          />
        </ScrollAnchor>
      );
    });
  }

  @bind
  handleCollapseClick() {
    this.setState({
      'isCollapsed': !this.state.isCollapsed
    });
  }

  renderContent(tracks) {
    if (this.state.isCollapsed) return null;

    return (
      <div>
        <InfiniteScrollList
          resourceList={tracks}
          renderList={this.renderTracks}
          scrollToBottom={false}
          buttonType="popoverPrimary"
        />
      </div>
    )
  }

  render() {
    const
    {item} = this.props;

    const classNameObj = {
      'isCollapsed': this.state.isCollapsed,
    };

    return (
      <div className={classNames(classNameObj)}>
        <div className={classNames('header')}>
          <div className={classNames('name')}>{item.name}</div>
          <div className={classNames('length')}>{item.count} tracks</div>
          <div className={classNames('actions')}>
            <Button
              icon="app.chevron"
              type="queueItemButton queueItemCollapseButton"
              onClick={this.handleCollapseClick}
            />
          </div>
        </div>
        {this.renderContent(item.tracks)}
      </div>
    );
  }
}
