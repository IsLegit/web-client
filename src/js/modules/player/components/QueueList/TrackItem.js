import preact from 'preact';
import bem from 'bem-classname';
import {observer, inject} from 'mobx-react';

// Components
import ResourceImage from 'common/components/ResourceImage';

// HOC
import {injectPlayer} from 'modules/player/hoc';
import {wrapInLazyLoadHandler} from 'common/hoc';

const classNames = bem.bind(null, 'queueTrackItem');

const renderPlaceholder = () => {
  return (
    <div className="queueTrackItem__placeholder">
      <div className="queueTrackItem__placeholderArtwork"></div>
      <div className="queueTrackItem__placeholderBody"></div>
    </div>
  )
}

@wrapInLazyLoadHandler(renderPlaceholder)
@injectPlayer
@observer
export default class TrackItem extends preact.Component {
  @bind handleSelect() {
    const {player, index, queueIndex} = this.props;

    player.goto(queueIndex, index);
  }

  render() {
    const {resource, isActive} = this.props;

    return (
      <div onClick={this.handleSelect} className={classNames({
        'isActive': isActive
      })}>
        <div className={classNames('artwork')}>
          <ResourceImage resource={resource} cheap={true} name="artwork" size="small"/>
        </div>
        <div className={classNames('content')}>
          <div className={classNames('title')}>{resource.data.title}</div>
          <div className={classNames('author')}>{resource.user.data.display_name}</div>
        </div>
      </div>
    )
  }
}
