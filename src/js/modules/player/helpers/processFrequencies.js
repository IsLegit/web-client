export function logarithmicArray(array) {
    var logify, exp, i, logindex, len, low, high, lv, hv, w, v, result;
    logify = function (value, min, max) {
        exp = (value - min) / (max - min);
        return min * Math.pow(max / min, exp);
    };
    len = array.length;
    result = [];
    for (i = 0; i < len; i += 1) {
        logindex = logify(i, 1, array.length);

        low = Math.floor(logindex);
        high = Math.ceil(logindex);

        lv = array[low];
        hv = array[high];

        w = (logindex - low) / (high - low);
        v = lv + (hv - lv) * w;
        if (low === high) {
            v = array[low];
        }
        result.push(v);
    }
    return result;
}

export function resizeArray(array, integer) {
    var length = array.length,
        start = array[0],
        factor = length / integer;

    return Array.apply(null, {
        length: integer
    }).map(function (_, i) {
        return array[Math.floor(i * factor)];
    });
}

export function smoothArray(array, parts) {
    function easeInOutQuad(x, t, b, c, d) {
        'use strict';
        if ((t /= d / 2) < 1) {
            return c / 2 * t * t + b;
        } else {
            return -c / 2 * ((t -= 1) * (t - 2) - 1) + b;
        }
    }

    function fn(f, x, v) {
        return f(x, 1000 * x, 0, v, 1000);
    }

    var i;
    array = array.slice();
    for (i = 0; i < parts; i += 1) {
        array[i] = fn(easeInOutQuad, i / parts, array[i]) || 0;
        array[array.length - 1 - i] = fn(easeInOutQuad, i / parts, array[array.length - 1 - i]) || 0;
    }
    return array;
}
