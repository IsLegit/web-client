import BasicAudioSource from './BasicAudioSource';
import AudioProcessedSource from './AudioProcessedSource';

function getAudioSource() {
  if (window.AudioContext) return AudioProcessedSource;

  return BasicAudioSource;
}

const AudioSource = getAudioSource();
export default AudioSource;
