import BasicAudioSource from './BasicAudioSource';
import AudioProcessor from 'modules/player/classes/AudioProcessor';

export default class AudioProcessedSource extends BasicAudioSource {
  constructor() {
    super();

    // Set cors to allow reading
    this.audio.preload = 'auto';
    this.audio.crossOrigin = 'anonymous';
    this.audio.volume = 0.3;

    this.processor = new AudioProcessor(this.audio);
  }

  setVolume(volume) {
    this.processor.gain.gain.value = volume * 3
    this.audio.volume = 0.3;
  }
}
