import {observable, action, computed, observe} from 'mobx';
import QueueItem from './QueueItem';

export default class Queue {
  @observable items = [];
  @observable index = 0;

  constructor() {
    this.restoreQueue();
  }

  @action add(name, endpoint, data) {
    this.items.forEach

    this.items.unshift(new QueueItem(name, endpoint, data));
    this.index = 0;

    this.saveQueue();
  }

  @action setIndex(index, itemIndex) {
    const
    newIndex = Math.min(Math.max(parseInt(index), 0), this.items.length);

    this.index = newIndex;
    this.currentItem.setIndex(itemIndex);
    this.saveQueue();

    return this.currentTrack;
  }

  @action goNext() {
    const
    nextIndex = this.index + 1,
    hasMore = this.indexFromEnd > 0,
    nextTrack = this.currentItem.goNext();

    if (nextTrack) {
      this.saveQueue();
      return nextTrack;
    }

    if (hasMore) {
      this.index = nextIndex;
      this.saveQueue();

      return this.currentTrack;
    }

    return null;
  }

  @action goPrevious() {
    const
    previousIndex = this.index - 1,
    previousTrack = this.currentItem.goPrevious();

    if (previousTrack) {
      this.saveQueue();
      return previousTrack;
    }

    if (previousIndex >= 0) {
      this.index = previousIndex;
      this.saveQueue();

      return this.currentTrack;
    }

    return null;
  }

  // Save and restore from localStorage
  saveQueue() {
    const
    currentItem = this.currentItem;

    if (currentItem) localStorage.setItem('playerQueue', JSON.stringify(currentItem.localStorageData));
  }

  restoreQueue() {
    const
    restoredItemJSON = localStorage.getItem('playerQueue');

    if (restoredItemJSON) {
      const
      restoredItem = JSON.parse(restoredItemJSON);
      this.add(restoredItem.name, restoredItem.url, restoredItem.data);
    }
  }

  @computed get currentItem() {
    if (this.items.length > 0) return this.items[this.index];
    return null;
  }

  @computed get currentTrack() {
    const
    currentItem = this.currentItem;

    if (currentItem) return currentItem.currentItem;
    return null;
  }

  @computed get indexFromEnd() {
    return Math.abs((this.items.length - 1) - this.index);
  }
}
