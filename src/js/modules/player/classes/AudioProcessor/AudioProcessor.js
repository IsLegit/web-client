import AnalyserNodeGroup from './AnalyserNodeGroup';

export default class AudioProcessor {
  context = null;
  source = null;

  constructor(sourceAudio) {
    this.context = new AudioContext();
    this.source = this.context.createMediaElementSource(sourceAudio);

    // Nodes
    this.analyser = new AnalyserNodeGroup(this.context);
    this.gain = this.context.createGain();

    // Connection!
    this.connectNodes();
  }

  connectNodes() {
    this.analyser.connectInput(this.source);
    this.analyser.connectOutput(this.gain);

    this.gain.connect(this.context.destination);
  }
}
