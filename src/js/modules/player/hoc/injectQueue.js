import preact from 'preact';

const injectQueue = (getQueueData) => (Component) => {
  class QueueContext extends preact.Component {
    getChildContext() {
      const
      queueData = getQueueData(this.props);

      if (queueData) return {
        'playerQueue': queueData,
      };

      return null;
    }

    render() {
      return <Component {...this.props}/>;
    }
  }

  return QueueContext;
}

export default injectQueue;
