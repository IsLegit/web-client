import preact from 'preact';
import {observer, inject} from 'mobx-react';
import Player from 'player';

const injectPlayer = (Component) => {
  @observer
  class PlayerInjector extends preact.Component {
    render() {
      // Reference observables to trigger re-render when they change
      const {
        isPlaying,
        isBuffering,
        hasFailed,
        currentTrack,
        volume,
      } = Player;

      return (
        <Component {...this.props} player={Player}/>
      )
    }
  }

  return PlayerInjector;
}

export default injectPlayer;
