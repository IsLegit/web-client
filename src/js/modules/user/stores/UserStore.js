import {ReferencedResourceStore} from 'common/classes/Store';
import {UserModel} from 'modules/user/models';

class UserStore extends ReferencedResourceStore {
  Model = UserModel

  getReferenceName(data) {
    return data.username
  }
}

const store = new UserStore('/users', 'UserStore');
export default store;
