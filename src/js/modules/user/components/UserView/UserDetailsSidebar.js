import preact from 'preact';

// Components
import ResourceImage from 'common/components/ResourceImage';

export default class UserDetailsSidebar extends preact.Component {
  render() {
    const
    {resource} = this.props,
    {data} = resource;

    const avatarImageProps = {
      'width': '100%',
      'height': '100%',
      'resource': resource,
      'name': 'avatar',
      'size': 'auto',
    };

    return (
      <div className="userView__sidebar">
        <div className="userView__userBadge">
          <div className="userView__avatar">
            <ResourceImage {...avatarImageProps}/>
          </div>
          <div className="userView__badgeFooter">
            <div className="userView__displayName">{data.display_name}</div>
          </div>
        </div>
      </div>
    )
  }
}
