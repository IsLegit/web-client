import preact from 'preact';

// Components
import ResourceImage from 'common/components/ResourceImage';

export default class UserDetailsHeader extends preact.Component {
  render() {
    const
    {resource} = this.props,
    {data} = resource;

    const avatarImageProps = {
      'width': '100%',
      'height': '100%',
      'resource': resource,
      'name': 'avatar',
      'size': 'auto',
    };

    return (
      <div className="userView__header">
        <div className="userView__headerTop">
          <div className="userView__avatar">
            <ResourceImage {...avatarImageProps}/>
          </div>
          <div className="userView__primaryInfo">
            <div className="userView__displayName">{data.display_name}</div>
            <div className="userView__type">{data.type_display}</div>
          </div>
        </div>
      </div>
    )
  }
}
