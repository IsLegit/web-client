import preact from 'preact';
import {observer, inject} from 'mobx-react';

// Hoc
import {wrapInMediaQuery, injectResource} from 'common/hoc/';

// Components
import ResourceImage from 'common/components/ResourceImage';
import {LinkedTabs} from 'common/components/Tabs';

// Related components
import UserDetailsHeader from './UserDetailsHeader';
import UserDetailsSidebar from './UserDetailsSidebar';

import UserTrackList from './UserView__TrackList';

// Routing
import {Route, Redirect, Switch} from 'react-router-dom';
import {matchUrl} from 'common/helpers/routing';

const UserDetails = wrapInMediaQuery('(max-width: 1090px)')(UserDetailsHeader, UserDetailsSidebar);

@injectResource('UserStore', (props) => {
  return props.UserStore.fetchItemByReference(props.match.params.username);
}) @observer
export default class UserView extends preact.Component {
  renderSidebar(data, resource) {
    const avatarImageProps = {
      'width': '100%',
      'height': '100%',
      'resource': resource,
      'name': 'avatar',
      'size': 'auto',
    };

    return (
      <div className="userView__sidebar">
        <div className="userView__userBadge">
          <div className="userView__avatar">
            <ResourceImage {...avatarImageProps}/>
          </div>
          <div className="userView__badgeFooter">
            <div className="userView__displayName">{data.display_name}</div>
          </div>
        </div>
        {this.renderTags(data)}
      </div>
    )
  }

  renderTags(data) {
    return !data.country ?
      (
        <div className="userView__secondaryInfo">
          <h2 className="userView__type">{data.type_display}</h2>
        </div>
      ) : (
        <div className="userView__secondaryInfo">
          <h2 className="userView__type">{data.type_display}</h2>
          <div className="userView__secondaryInfoDivider"></div>
          <div className="userView__country">{data.country}</div>
        </div>
      )
  }

  renderContent(data, resource) {
    const tabs = [
      {path: `${resource.clientlink}/tracks`, label: 'Tracks'},
      {path: `${resource.clientlink}/collections`, label: 'Collections'},
    ];

    return (
      <div className="userView__content">
        <div className="userView__contentHeader">
          <div className="userView__tabs">
            <LinkedTabs tabs={tabs}/>
          </div>
        </div>
        <div className="userView__contentBody">
          <Switch>
            <Route path={tabs[0].path}>
              <UserTrackList resource={resource}></UserTrackList>
            </Route>
          </Switch>
        </div>
      </div>
    )
  }

  render() {
    const
    {resource, match, location} = this.props,
    {data, media} = resource;

    const shouldRedirect = matchUrl(location.pathname, resource.clientlink);

    // Redirect if necessary (THIS SHOULD BE REFACTORED)
    if (shouldRedirect) {
      return <Redirect to={`${resource.clientlink}/tracks`}/>
    }

    return (
      <div className="userView">
        <UserDetails {...this.props}/>
        {this.renderContent(data, resource)}
      </div>
    )
  }
}
